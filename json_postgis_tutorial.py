# Convert regular JSON file to PostGIS records
import urllib.request
import pandas as pd
import geopandas as gpd
from shapely.geometry import Point
from geoalchemy2 import Geometry, WKTElement
from sqlalchemy import create_engine


class JSONtoPostGIS:
    def __init__(self):
        self.json_source = ''    # JSON URL
        self.df = pd.DataFrame()   # JSON dataframe

    def set_source(self, json_source):
        self.json_source = json_source

    def read_json(self):
        data = urllib.request.urlopen(self.json_source).read()
        self.df = pd.read_json(data)
        return self.df

    def create_gdf(self, type, longitude='longitude', latitude='latitude',
                   geom_col='geom'):
        self.geom_col = geom_col
        if type == 'point':
            self.df[geom_col] = list(zip(self.df[longitude], self.df[latitude]))
            self.df[geom_col] = self.df[geom_col].apply(Point)
        self.gdf = gpd.GeoDataFrame(self.df, geometry=geom_col)

    def drop_columns(self, columns_to_drop):
        if not isinstance(columns_to_drop, list):
            raise TypeError('columns_to_drop must be a list')
        for column in columns_to_drop:
            self.gdf = self.gdf.drop([column], axis=1)

    def transform_to_wkt(self):
        self.gdf[self.geom_col] = self.gdf[self.geom_col].apply(create_wkt_element)

    def to_postgis(self, type, engine, table_name, schema, if_exists='append'):
        self.gdf.to_sql(table_name, engine, schema=schema, if_exists=if_exists,
                        index=False,
                        dtype={self.geom_col: Geometry(type, srid=4326)})

def create_wkt_element(geom):
    return WKTElement(geom.wkt, srid=4326)


if __name__ == '__main__':
    JSON_URL = 'https://data.cityofchicago.org/resource/4ywc-hr3a.json'
    jtp = JSONtoPostGIS()
    jtp.set_source(JSON_URL)
    jtp.read_json()
    jtp.create_gdf(type='point', longitude='longitude', latitude='latitude')
    jtp.drop_columns(['location'])
    jtp.transform_to_wkt()
    engine = create_engine('{}://{}:{}@{}:{}/{}'.format(
        'postgresql',
        'user',
        'password',
        'localhost',
        5433,
        'db',
    ))
    jtp.to_postgis('POINT', engine, 'chicago_rack', 'bicycle')
