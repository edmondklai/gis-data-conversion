import geopandas as gpd
import psycopg2


class Geometry:
    def __init__(self, gdf):
        self.gdf = gdf

    @classmethod
    def from_postgis(cls, connection, sql, geom_col='geom'):
        with psycopg2.connect(**connection) as conn:
            gdf = gpd.GeoDataFrame.from_postgis(sql, conn, geom_col=geom_col)
        return cls(gdf)


class Polygons(Geometry):
    def __init__(self, gdf):
        super().__init__(gdf)

    def points_within(self,
                      points,
                      out_col='within_count',
                      polygon_geom_col='geom',
                      point_geom_col='geom'):
        self.gdf[out_col] = 0
        for poly_index, data in self.gdf.iterrows():
            count = 0
            polygon_geom = data[polygon_geom_col]
            for point_index, data in points.gdf.iterrows():
                point_geom = data[point_geom_col]
                if point_geom.within(polygon_geom):
                    count += 1
            self.gdf.loc[poly_index, out_col] = count


class Points(Geometry):
    def __init__(self, gdf):
        super().__init__(gdf)


if __name__ == '__main__':
    POLYGONS_SQL = "SELECT * FROM boundary.neighborhood"

    POINTS_SQL = "SELECT * FROM bicycle.rack"

    DB_CONN = {
        'dbname':'db',
        'user':'user',
        'host':'localhost',
        'password':'password',
        'port':5433
    }

    community = Polygons.from_postgis(DB_CONN, POLYGONS_SQL)
    racks = Points.from_postgis(DB_CONN, POINTS_SQL)
    community.points_within(racks)
